//models/index.js
const Fs = require("fs");
const Path = require("path");
const Sequelize = require("sequelize");
const Config = require('../config/index');

let db = {};

//创建一个sequelize对象实例,连接数据库
let sequelize = new Sequelize(Config.db_name, Config.db_username, Config.db_password, {
    host: Config.db_host,
    port: Config.db_port,
    dialect: Config.db_dialect,
    define: {
        timestamps: false,
        createdAt: false,
        updatedAt: false
    },
    pool: {
        max: 5,
        min: 0,
        idle: 30000
    }
});

Fs.readdirSync(__dirname).filter(function (file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
}).forEach(function (file) {
    var model = sequelize["import"](Path.join(__dirname, file));
    db[model.name] = model;
});

db.sequelize = sequelize;
module.exports = db;