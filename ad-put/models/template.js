/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Template', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    code: {
      type: DataTypes.STRING(4000),
      allowNull: false
    },
    element: {
      type: DataTypes.STRING(2000),
      allowNull: false
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    create_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    update_by: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'ads_template'
  });
};
