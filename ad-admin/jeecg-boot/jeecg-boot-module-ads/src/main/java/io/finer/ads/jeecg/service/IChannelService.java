package io.finer.ads.jeecg.service;

import io.finer.ads.jeecg.entity.Channel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 广告频道
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface IChannelService extends IService<Channel> {

}
